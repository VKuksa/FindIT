from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, RadioField
from wtforms.validators import DataRequired
from findit.models import english_levels


class VacancyForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    city = StringField('City', validators=[DataRequired()])
    experience = StringField('Required experience', validators=[DataRequired()])
    skills = StringField('Required skills', validators=[DataRequired()])
    english_level = RadioField('Label', choices=english_levels)
    description = TextAreaField('Description', validators=[DataRequired()])
    position = StringField('Position', validators=[DataRequired()])
    salary = StringField('Salary', validators=[DataRequired()])
    submit = SubmitField('Submit')




