from flask import (render_template, url_for, flash,
                   redirect, request, abort, Blueprint)
from flask_login import current_user, login_required
from findit import db
from findit.models import Vacancy
from findit.vacancies.forms import VacancyForm
from findit.search import find_suitable_users
from findit.config import Config

vacancies = Blueprint('vacancies', __name__)


@vacancies.route("/vacancy/new", methods=['GET', 'POST'])
@login_required
def new_vacancy():
    if request.method == 'POST':
        entries = {}

        for key, value in request.form.items():
            if not value == '':
                entries[key] = value

        vacancy = Vacancy(**entries)
        vacancy.author = current_user
        db.session.add(vacancy)
        db.session.commit()
        flash('Вашу вакансію було додано!', 'success')
        return redirect(url_for('main.vacancies'))
    return render_template('create_vacancy.html', title='New vacancy', legend='Submit vacancy', backend_url=Config.WEBPAGE_URL)


@vacancies.route("/vacancy/<int:vacancy_id>")
def preview_vacancy(vacancy_id):
    vacancy = Vacancy.query.get_or_404(vacancy_id)
    users = find_suitable_users(vacancy)
    return render_template('vacancy.html', title=vacancy.title, vacancy=vacancy, users=users)


@vacancies.route("/vacancy/<int:vacancy_id>/update", methods=['GET', 'POST'])
@login_required
def update_vacancy(vacancy_id):
    vacancy = Vacancy.query.get_or_404(vacancy_id)
    if vacancy.author != current_user:
        abort(403)
    form = VacancyForm()
    if form.validate_on_submit():
        vacancy.title = form.title.data,
        vacancy.city = form.city.data,
        vacancy.experience = form.experience.data,
        vacancy.skills = form.skills.data,
        vacancy.english_level = form.english_level.data,
        vacancy.description = form.description.data
        db.session.commit()
        flash('Your vacancy has been updated!', 'success')
        return redirect(url_for('vacancies.preview_vacancy', vacancy_id=vacancy.id))
    elif request.method == 'GET':
        vacancy.title = form.title.data,
        vacancy.city = form.city.data,
        vacancy.experience = form.experience.data,
        vacancy.skills = form.skills.data,
        vacancy.english_level = form.english_level.data,
        vacancy.description = form.description.data
    return render_template('create_vacancy.html', title='Update Vacancy', form=form, legend='Update Vacancy')


@vacancies.route("/vacancy/<int:vacancy_id>/delete", methods=['POST'])
@login_required
def delete_vacancy(vacancy_id):
    vacancy = Vacancy.query.get_or_404(vacancy_id)
    if vacancy.publisher != current_user:
        abort(403)
    db.session.delete(vacancy)
    db.session.commit()
    flash('Вашу вакансію було видалено!', 'success')
    return redirect(url_for('main.home'))
