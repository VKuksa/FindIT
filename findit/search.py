from findit.models import User, Vacancy
import operator


def find_suitable_vacancies(user: User):
    user_skills = user.skills.split(';')
    user_skills.pop()

    query = Vacancy.query.filter(Vacancy.publisher != user.id).all()
    vacancies = {}

    for vacancy in query:
        print("INFO: find_suitable_vacancies USER: {0} {1} VACANCY {2} {3}".format(user.city, user.position, vacancy.city, vacancy.position))
        if is_same_city(user.city, vacancy.city) and is_same_position(user.position, vacancy.position):
            current_vacancy_coeff = 1
            try:
                vacancy_skills = vacancy.skills.split(';')
                vacancy_skills.pop()
                current_vacancy_coeff *= get_coefficient_skilled(vacancy_skills, user_skills)
                current_vacancy_coeff *= get_coefficient_salary(user.expected_salary, vacancy.provided_salary)
                current_vacancy_coeff *= get_coefficient_english(user.english_level, vacancy.english_level)
                current_vacancy_coeff *= get_coefficient_experience(int(user.experience), int(vacancy.experience))
                vacancies[vacancy] = current_vacancy_coeff
            except:
                print("WARN: find_suitable_vacancies dropped exception with {0} and {1}".format(user, vacancy))
                continue

    sorted_vacancies = sorted(vacancies.items(), key=operator.itemgetter(1))
    result = []

    for vacancy in sorted_vacancies[:5]:
        print("INFO: {0} : {1}".format(vacancy[0], vacancy[1]))
        result.append(vacancy[0])
    return result


def find_suitable_users(vacancy: Vacancy):
    vacancy_skills = vacancy.skills.split(';')
    vacancy_skills.pop()

    query = User.query.filter(User.id != vacancy.publisher).all()

    users = {}

    for user in query:
        print("INFO: find_suitable_users USER: {0} {1} VACANCY {2} {3}".format(user.city, user.position, vacancy.city, vacancy.position))
        if user.has_city_and_position() \
                and is_same_city(user.city, vacancy.city) \
                and is_same_position(user.position, vacancy.position):
            current_user_coeff = 1
            try:
                user_skills = user.skills.split(';')
                user_skills.pop()
                current_user_coeff *= get_coefficient_skilled(user_skills, vacancy_skills)
                current_user_coeff *= get_coefficient_salary(user.expected_salary, vacancy.provided_salary)
                current_user_coeff *= get_coefficient_english(user.english_level, vacancy.english_level)
                current_user_coeff *= get_coefficient_experience(int(user.experience), int(vacancy.experience))
                users[user] = current_user_coeff
            except:
                print("WARN: find_suitable_users dropped exception with {0} and {1}".format(vacancy, user))
                continue

    sorted_users = sorted(users.items(), key=operator.itemgetter(1))
    result = []

    for user in sorted_users[:5]:
        print("INFO: {0} : {1}".format(user[0], user[1]))
        result.append(user[0])
    return result


def get_coefficient_skilled(available_skills: list, required_skills: list):
    coefficient_skilled = 1 / len(required_skills)
    result = 0
    for skill in required_skills:
        if skill in available_skills:
            result += coefficient_skilled
    return result


def is_same_city(available_city: str, required_city: str):
    return available_city == required_city


def get_coefficient_salary(expected_salary: int, salary: int):
    coefficient_salary = expected_salary / salary
    if 0 <= coefficient_salary <= 1:
        return coefficient_salary
    elif 1 < coefficient_salary < 2:
        return 2 - coefficient_salary
    else:
        return 0


def is_same_position(student_employment_position: list, position: list):
    return position in student_employment_position


def get_coefficient_option(student_employment_option, employment_option):
    if student_employment_option == employment_option:
        return 1
    else:
        return 0.3


def get_coefficient_english(student_level_english, desired_level_english):
    english_level = {
        'No English': 0.05,
        'Beginner/Elementary': 0.1,
        'Pre-Intermediate': 1,
        'Intermediate': 2,
        'Upper-Intermediate': 3,
        'Advanced/Fluent': 4,
    }
    student_coeff = english_level[student_level_english]
    desired_coeff = english_level[desired_level_english]
    if student_coeff >= desired_coeff:
        return 1
    else:
        return student_coeff / desired_coeff


def get_coefficient_experience(students_experience: int, required_exp: int):
    if students_experience >= required_exp:
        return 1
    return 1 * (required_exp - students_experience)
