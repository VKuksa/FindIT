from flask import render_template, url_for, flash, redirect, request, Blueprint
from flask_login import login_user, current_user, logout_user, login_required
from findit import db, bcrypt
from findit.models import User, Vacancy
from findit.users.forms import (RegistrationForm, LoginForm, UpdateContactInfoForm,
                                RequestResetForm, ResetPasswordForm)
from findit.users.utils import save_picture, send_reset_email
from findit.search import find_suitable_vacancies
from findit.config import Config

users = Blueprint('users', __name__)


@users.route("/suggested_vacancies")
@login_required
def suggest_vacancies():
    vacancies = find_suitable_vacancies(current_user)
    return render_template('suggested_vacancies.html', title='Find your vacancy', vacancies=vacancies)


@users.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(name=form.name.data, surname=form.surname.data, email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash('Ваш аккаунт було створено! Ви маєте можливість увійти в систему', 'success')
        return redirect(url_for('users.login'))
    return render_template('register.html', title='Register', form=form)


@users.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('main.home'))
        else:
            flash('Вхід в систему був не успішний. Будь ласка, перевірте правильність вводу електронної пошти та паролю', 'danger')
    return render_template('login.html', title='Login', form=form)


@users.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('main.home'))


@users.route("/profile", methods=['GET', 'POST'])
@login_required
def profile():
    if request.method == 'POST':
        updated_entities = {}

        for key, value in request.form.items():
            if not value == '':
                updated_entities[key] = value

        User.query.filter_by(id=current_user.id).update(updated_entities)
        db.session.flush()
        db.session.commit()
        flash('Інформація по вашому профілю було оновлено!', 'success')
        return redirect(url_for('users.profile'))
    return render_template('profile.html', title='Profile', backend_url=Config.WEBPAGE_URL)


@users.route("/contact_info", methods=['GET', 'POST'])
@login_required
def contact_info():
    form = UpdateContactInfoForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        current_user.email = form.email.data
        current_user.telephone = form.phone.data
        current_user.skype = form.skype.data
        current_user.github = form.github.data
        db.session.commit()
        flash('Вашу контактну інформацію було оновлено!', 'success')
        return redirect(url_for('users.contact_info'))
    elif request.method == 'GET':
        form.email.data = current_user.email
        form.phone.data = current_user.telephone
        form.skype.data = current_user.skype
        form.github.data = current_user.github
    image_file = url_for('static', filename='profile_pics/' + current_user.image_file)
    return render_template('contact_info.html', title='Update contact info',
                           image_file=image_file, form=form)


@users.route("/user/<string:id>")
def user(id):
    page = request.args.get('page', 1, type=int)
    user = User.query.filter_by(id=id).first_or_404()
    vacancies = Vacancy.query.filter_by(author=user) \
        .order_by(Vacancy.date_posted.desc()) \
        .paginate(page=page, per_page=5)
    image_file = url_for('static', filename='profile_pics/' + user.image_file)
    return render_template('user.html', vacancies=vacancies, user=user, image_file=image_file)


@users.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        flash('An email has been sent with instructions to reset your password.', 'info')
        return redirect(url_for('users.login'))
    return render_template('reset_request.html', title='Reset Password', form=form)


@users.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('main.home'))
    user = User.verify_reset_token(token)
    if user is None:
        flash('That is an invalid or expired token', 'warning')
        return redirect(url_for('users.reset_request'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user.password = hashed_password
        db.session.commit()
        flash('Your password has been updated! You are now able to log in', 'success')
        return redirect(url_for('users.login'))
    return render_template('reset_token.html', title='Reset Password', form=form)
