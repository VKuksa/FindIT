from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from findit import db, login_manager
from flask_login import UserMixin


english_levels = [("1", "No English"),
                  ("2", "Beginner/Elementary"),
                  ("3", "Pre-Intermediate"),
                  ("4", "Intermediate"),
                  ("5", "Upper-Intermediate"),
                  ("6", "Advanced/Fluent")]
employment_types = ["Робота в офісі на повний день", "Віддалена робота (повний день)", "Фріланс (разові проекти)", "Переїзд в інше місто України", "Relocate до США чи Європи"]
technology_titles = ["C/C++", "C++", "C", "JavaScript", "Java", "Unreal Engine 4", "Unity", "C#",
                     "SQL", "Git", "Mercurial", "Ubuntu", "Windows", "Go", "Python", "Angular", "React",
                     "Boost", "Lisp", "Flask", "Django", "PostgreSQL", "Typescript", "Objective-C", "Swift",
                     "Hybernate", "Machine Learning", "Big Data", "AI", "Ruby", "R", "F#", "Visual Basic", "HTML",
                     "CSS", "Bootstrap", "Bash", "Elixir", "Lisp", "Fortran", "Haskell", "Kotlin", "REST API",
                     "gRPC", "SQLite", "MATLAB", "Pascal", "Rust", "CMake", "Make"]
city_names = ["Lviv", "Kyiv", "Berlin", "Wien", "Moscow", "Kharkiv", "London", "Prague", "Rome"]


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)
    surname = db.Column(db.String(20), nullable=False)
    email = db.Column(db.String(120), nullable=False)
    telephone = db.Column(db.String(20), default="")
    skype = db.Column(db.String(60), default="")
    github = db.Column(db.String(50), default="")
    department = db.Column(db.String(255), default="")
    skills = db.Column(db.String(255), default="")
    image_file = db.Column(db.String(20), default='default.jpg')
    employment_type = db.Column(db.String(255), default="")
    city = db.Column(db.String(255), default="")
    password = db.Column(db.String(60), nullable=False)
    english_level = db.Column(db.String(255), default="")
    vacancies = db.relationship('Vacancy', backref='author', lazy=True)
    description = db.Column(db.Text, default="")
    position = db.Column(db.String(255), default="")
    experience = db.Column(db.Integer, default=0)
    desired_position = db.Column(db.String(255), default="")
    expected_salary = db.Column(db.Integer, default=0)

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'surname': self.surname,
            'email': self.email,
            'telephone': self.telephone,
            'skype': self.skype,
            'department': self.department,
            'skills': self.skills,
            'image_file': self.image_file,
            'employment_type': self.employment_type,
            'city': self.city,
            'password': self.password,
            'english_level': self.english_level,
            'vacancies': self.vacancies,
            'description': self.description,
            'position': self.position,
            'experience': self.experience
        }

    def has_city_and_position(self):
        return self.position != "" and self.city != ""

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)

    def __repr__(self):
        return f"User('{self.name}', '{self.surname}', '{self.email}', '{self.image_file}')"

    def __init__(self, **entries):
        self.__dict__.update(entries)

    def __update__(self, key, value):
        self.__dict__[key] = value


class Vacancy(db.Model):
    __tablename__ = 'vacancy'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False, default="")
    city = db.Column(db.String(255), nullable=False, default="")
    experience = db.Column(db.Integer, nullable=False, default=0)
    skills = db.Column(db.String(255), nullable=False, default="")
    english_level = db.Column(db.String(30), nullable=False, default="")
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    description = db.Column(db.Text, nullable=False, default="")
    position = db.Column(db.String(255), nullable=False, default="")
    provided_salary = db.Column(db.Integer, nullable=False, default=0)
    company_name = db.Column(db.String(255), default="Unspecified")
    company_page = db.Column(db.String(255), default="")
    short_description = db.Column(db.String(255), default="")
    about_company = db.Column(db.Text, default="")
    publisher = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return f"Vacancy('{self.title}', '{self.date_posted}', '{self.city}', '{self.experience}', '{self.skills}')"

    def __init__(self, **entries):
        self.__dict__.update(entries)
