from flask import render_template, Blueprint, request, url_for, redirect, jsonify, flash
from flask_login import login_required, current_user
from findit.models import Vacancy, User, city_names, employment_types, english_levels, technology_titles
from findit import db

main = Blueprint('main', __name__)


@main.route("/admin/create_tables")
def create_tables():
    try:
        db.create_all()
        flash('Database fields were created!', 'success')
    except:
        flash('An error occurred during creation!', 'failure')
    return render_template('home.html', title='Home')


@main.route("/tutorial")
def tutorial():
    return render_template('tutorial_layout.html', title='Tutorial')


@main.route("/admin/drop_tables")
def drop_tables():
    try:
        db.drop_all()
        flash('Database fields were dropped!', 'success')
    except:
        flash('An error occurred during dropping!', 'failure')
    return render_template('home.html', title='Home')


@main.route("/about")
def about():
    return render_template('about.html', title='About')


@main.route("/")
@main.route("/home")
def home():
    if current_user.is_authenticated:
        return redirect(url_for('main.vacancies'))
    return render_template('home.html', title='Home')


@main.route("/cities")
def cities():
    response = jsonify(city_names)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@main.route("/employment_types")
def employment_types():
    response = jsonify(employment_types)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@main.route("/english_levels")
def english_levels():
    response = jsonify(english_levels)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@main.route("/technologies")
def technologies():
    response = jsonify(technology_titles)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@main.route("/vacancies")
@login_required
def vacancies():
    page = request.args.get('page', 1, type=int)
    vacancies = Vacancy.query.paginate(page=page, per_page=5)
    return render_template('vacancies.html', title='Vacancies', vacancies=vacancies)


@main.route("/candidates")
def candidates():
    page = request.args.get('page', 1, type=int)
    candidates = User.query.paginate(page=page, per_page=5)
    return render_template('candidates.html', title='Vacancies', candidates=candidates)
