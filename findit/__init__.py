from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_mail import Mail
from findit.config import Config


db = SQLAlchemy()
bcrypt = Bcrypt()
login_manager = LoginManager()
login_manager.login_view = 'users.login'
login_manager.login_message_category = 'info'
mail = Mail()


def init_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)

    from findit.users.routes import users
    from findit.vacancies.routes import vacancies
    from findit.main.routes import main
    from findit.errors.handlers import errors
    app.register_blueprint(users)
    app.register_blueprint(vacancies)
    app.register_blueprint(main)
    app.register_blueprint(errors)

    return app
